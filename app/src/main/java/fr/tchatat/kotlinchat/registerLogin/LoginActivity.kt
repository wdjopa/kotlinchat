package fr.tchatat.kotlinchat.registerLogin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import fr.tchatat.kotlinchat.R
import fr.tchatat.kotlinchat.messages.LatestMessagesActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(){
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()

        btn_connexion.setOnClickListener{
           connexion()
        }

        text_view_register_connexion.setOnClickListener{
            finish()
        }
    }

    private fun connexion(){
        val email = email_edit_text_connexion.text.toString()
        val password = password_edit_text_connexion.text.toString()

        if(email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Entrez vos informations", Toast.LENGTH_LONG).show()
            return
        }
        Log.d("Page Connexion", "Email : $email; password : $password")

        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
//                    val user = auth.currentUser
                    val intent = Intent(this, LatestMessagesActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("Page inscription", "createUserWithEmail:failure", it.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
//                        updateUI(null)
                }
            }
            .addOnFailureListener{
                Log.d("Page Inscription", "Erreur lors de la création d'un nouvel utilisateur. Message : ${it.message}")
                Toast.makeText(baseContext, "${it.message}",
                    Toast.LENGTH_SHORT).show()
            }
    }
}