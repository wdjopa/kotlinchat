package fr.tchatat.kotlinchat.registerLogin

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import fr.tchatat.kotlinchat.R
import fr.tchatat.kotlinchat.messages.LatestMessagesActivity
import fr.tchatat.kotlinchat.models.User
import kotlinx.android.synthetic.main.activity_register.*
import java.util.*

class RegisterActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        auth = FirebaseAuth.getInstance()

        profile_picture_btn.setOnClickListener{
            Log.d("Page d'inscription", "Selection de photo")
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }

        btn_inscription.setOnClickListener{
            inscription()
        }

        text_view_already_account.setOnClickListener{
            Log.d("Page isncription", "Tentative de connexion")

            // Lancement de la page de Login
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    var selectedPhotoUri : Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 0 && resultCode == Activity.RESULT_OK && data != null){
            Log.d("Register Activity", "Une photo a été sélectionnée")

            selectedPhotoUri = data.data


            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)

            profile_image.setImageBitmap(bitmap)
            profile_picture_btn.alpha = 0f

//            val bitmapDrawable = BitmapDrawable(bitmap)
//            profile_picture_btn.setBackgroundDrawable(bitmapDrawable)
        }
    }

    private fun inscription(){
        val username = username_edit_text_inscription.text.toString()
        val email = email_edit_text_inscription.text.toString()
        val password = password_edit_text_inscription.text.toString()

        if(email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Entrez vos informations", Toast.LENGTH_LONG).show()
            return
        }

        Log.d("Page Inscription", "Username $username; email : $email; password : $password")

        // Gestion de comptes avec Firebase

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("Page inscription", "createUserWithEmail:success")
                    uploadImageToFirebaseStorage()
                    val user = auth.currentUser
//                        updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("Page inscription", "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
//                        updateUI(null)
                }

                // ...
            }
            .addOnFailureListener{
                Log.d("Page Inscription", "Erreur lors de la création d'un nouvel utilisateur. Message : ${it.message}")
                Toast.makeText(baseContext, "${it.message}",
                    Toast.LENGTH_SHORT).show()
            }
    }

    private fun uploadImageToFirebaseStorage(){
        if(selectedPhotoUri == null) return

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")

        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                Log.d("Page inscription", "Image uploadée avec succès à l'adersse ${it.metadata?.path}")

                ref.downloadUrl.addOnSuccessListener {
                    Log.d("Page inscription", "File Location : $it")

                    saveUserToFirebase(it.toString())
                }
            }
            .addOnFailureListener{
                Log.d("Page inscription", "Erreur lors de l'upload de l'image")
            }
    }

    private fun saveUserToFirebase(profileImage : String){
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")

        val user = User(uid,username_edit_text_inscription.text.toString(), profileImage )
        ref.setValue(user)
            .addOnSuccessListener {
                Log.d("Page Inscription", "L'utilisateur a été inscrit")
                val intent = Intent(this, LatestMessagesActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
            .addOnFailureListener {
                Log.d("Page Inscription", "Erreur lors de l'inscription de l'utilisateur. ${it.message}")
            }
    }

/*
    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }*/
}
