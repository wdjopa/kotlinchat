package fr.tchatat.kotlinchat.models


class ChatMessage(val id:String, val fromId:String, val toId : String, val text:String, val timestamp:Long){
    constructor() : this("", "", "", "", -1)

    override fun toString(): String {
        return "Message $id. Envoyé par $fromId à $toId le $timestamp. Contenu : $text"
    }
}