package fr.tchatat.kotlinchat.views

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import fr.tchatat.kotlinchat.R
import fr.tchatat.kotlinchat.models.ChatMessage
import fr.tchatat.kotlinchat.models.User
import kotlinx.android.synthetic.main.latest_message_row.view.*


class LatestMessageRow(val chatMessage : ChatMessage): Item<GroupieViewHolder>(){
    var chatPartnerUser : User?=null

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {

        val chatPartner : String

        if(chatMessage.fromId == FirebaseAuth.getInstance().uid){
            chatPartner = chatMessage.toId
            viewHolder.itemView.text_view_latest_message_lastmessage.text = "Vous : "+chatMessage.text
        }
        else{
            chatPartner = chatMessage.fromId
            viewHolder.itemView.text_view_latest_message_lastmessage.text = chatMessage.text
        }

        val ref= FirebaseDatabase.getInstance().getReference("users/$chatPartner")
        ref.addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                chatPartnerUser = p0.getValue(User::class.java)
                viewHolder.itemView.text_view_latest_message_username.text = chatPartnerUser?.username

                val targetImageView = viewHolder.itemView.text_view_latest_message_profile_picture
                Picasso.get().load(chatPartnerUser?.profileImage).into(targetImageView)
            }
            override fun onCancelled(p0: DatabaseError) {

            }
        })

    }

    override fun getLayout(): Int {
        return R.layout.latest_message_row
    }
}

