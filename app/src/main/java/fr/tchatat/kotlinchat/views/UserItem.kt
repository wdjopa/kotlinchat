package fr.tchatat.kotlinchat.views

import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import fr.tchatat.kotlinchat.R
import fr.tchatat.kotlinchat.models.User
import kotlinx.android.synthetic.main.user_row_new_message.view.*


class UserItem(val user: User): Item<GroupieViewHolder>(){
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.new_message_list_username.text = user.username

        Picasso.get().load(user.profileImage).into(viewHolder.itemView.pp_new_message)
    }

    override fun getLayout(): Int {
        return R.layout.user_row_new_message
    }
}